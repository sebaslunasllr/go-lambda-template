# go-lambda-template initialized

Project initilized using [go-lambda-creator](https://gitlab.com/sebaslunasllr/go-lambda-creator/-/tree/master).
To start the project locally write in the console: `sam local start-api`.
To build the project use `env GOOS=linux go build -ldflags="-s -w" -o bin/go-lambda-template cmd/go-lambda-template.go`.
For every change you'll need to rebuild the project using the command stated above.

## Before running

### Requisites

- [SAM](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-windows.html).
- [Docker](https://www.docker.com/).
